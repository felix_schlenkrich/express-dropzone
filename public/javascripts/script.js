$(document).ready(function() {
  console.log('Document ready');

  var options = {
    maxFiles: 10,
    maxFilesize: 10, // MB
    acceptedFiles: 'image/*',
    autoQueue: false,
  };
  var upload = new Dropzone('#upload-widget', options);

  upload.on('addedfile', function(file) {
    console.log('File added ...');
    console.log(file);

    
    var maxWidth  = 800;
    var maxHeight = 600;

    var reader = new FileReader();

    reader.addEventListener("load", function(event) {

      var source = new Image();
      source.src = event.target.result;

      source.addEventListener("load", function(event) {

        var width  = event.target.width;
        var height = event.target.height;

        if (width <= maxWidth && height <= maxHeight) {
          dropzone.enqueueFile(file);
          return;
        }

        if (width > height) {
          if (width > maxWidth) {
            height *= maxWidth / width;
            width = maxWidth;
          }
        } else {
          if (height > maxHeight) {
            width *= maxHeight / height;
            height = maxHeight;
          }
        }

        var canvas = document.createElement('canvas');
        canvas.width = Math.round(width);
        canvas.height = Math.round(height);

        var ctx = canvas.getContext("2d");
        ctx.drawImage(source, 0, 0, width, height);

        $('#processing').append(canvas);

        //var resizedFile = base64ToFile(canvas.toDataURL('image/jpeg', 0.7), file);

        //base64ToFile(canvas.toDataURL('image/jpeg', 0.7));

        var resizedFile = canvas.toBlob(function(blob) {
          console.log(blob);
          var origProps = [ 
            "upload", "status", "previewElement", "previewTemplate", "accepted"
          ];

          $.each(origProps, function(i, p) {
            blob[p] = file[p];
          });

          blob.lastModifiedDate = new Date();
          console.log(blob);
          var fileIndex = upload.files.indexOf(file);
          upload.files[fileIndex] = blob;

          upload.enqueueFile(blob);
        },'image/jpeg', 0.7);

        
      });
    });

    reader.readAsDataURL(file);

    
  });

});

function base64ToFile(dataURI, file) {

  if(dataURI.split(',')[0].indexOf('base64') !== -1 ) {
    byteString = atob(dataURI.split(',')[1]);
  } else {
    byteString = decodeURI(dataURI.split(',')[1]);
  }

  mimestring = dataURI.split(',')[0].split(':')[1].split(';')[0];

  var arrayBuffer = new ArrayBuffer(byteString.length);
  var intArray = new Uint8Array(arrayBuffer);

  for(var i = 0; i < byteString.length; i++) {
    intArray[i] = byteString.charCodeAt(i);
  }

  var newFile = new File([arrayBuffer], file.name, {type: mimestring});

  var origProps = [ 
    "upload", "status", "previewElement", "previewTemplate", "accepted"
  ];

  $.each(origProps, function(i, p) {
    newFile[p] = file[p];
  });

  newFile.lastModifiedDate = new Date();


  console.log(newFile);
  return newFile;
}

function base64ToBlob(dataURI, file) {
  var byteString, mimestring;

  if(dataURI.split(',')[0].indexOf('base64') !== -1 ) {
    byteString = atob(dataURI.split(',')[1]);
  } else {
    byteString = decodeURI(dataURI.split(',')[1]);
  }

  mimestring = dataURI.split(',')[0].split(':')[1].split(';')[0];

  var content = new Array();
  for (var i = 0; i < byteString.length; i++) {
    content[i] = byteString.charCodeAt(i);
  }

  var newFile = new File(
    [new Uint8Array(content)], file.name, {type: mimestring}
  );

  var origProps = [ 
    "upload", "status", "previewElement", "previewTemplate", "accepted"
  ];

  $.each(origProps, function(i, p) {
    newFile[p] = file[p];
  });

  return newFile;
}